﻿CREATE TABLE user(
id SERIAL PRIMARY KEY AUTO_INCREMENT ,
login_id varchar(255) UNIQUE  NOT NULL ,
name varchar(255) NOT NULL ,
birth_date DATE NOT NULL ,
password varchar(255) NOT NULL ,
create_date DATETIME NOT NULL ,
update_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO user VALUES (2,'a','a','1998-12-04','mayohonda',now(),now());

SELECT * FROM user;

SELECT * FROM user WHERE login_id = 'admin' and password = 'mayohonda';

SELECT * FROM user WHERE id = 1 and name = 'a' and birth_date ='1998-12-04' and create_date and update_date ;



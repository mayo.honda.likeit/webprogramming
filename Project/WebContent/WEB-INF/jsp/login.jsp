<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <title>ログイン画面</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
     integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2"
     crossorigin="anonymous">

    <style>
        div{
            text-align : center ;
        }

    </style>
</head>

<body>


   <div class="container">
   <h1>ログイン画面</h1>

	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

	<c:if test="${errmsg != null}" >
		    <div class="alert alert-danger" role="alert">
			  ${errmsg}
			</div>
		</c:if>

	<div class="card card-container">


        <form class="form-signin" action="LoginServlet" method="post">
          <span id="reauth-email" class="reauth-email"></span>
          <input type="text" name="loginId" id="inputLoginId" class="form-control" placeholder="ログインID" required autofocus>
          <input type="password" name="password" id="inputPassword" class="form-control" placeholder="パスワード" required>
          <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">ログイン</button>
        </form><!-- /form -->
      </div><!-- /card-container -->
    </div><!-- /container -->


</body>


</html>
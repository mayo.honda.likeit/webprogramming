<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>ユーザー一覧</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2"crossorigin="anonymous">

    <style>
        .abc{
             text-align : right ;
        }
        .touroku{
            text-align : right ;
        }
    </style>
  </head>

<body>
  <nav class="navbar navbar-light bg-light">
  <span class="navbar-brand mb-0 h1">${userInfo.name} さん</span>
      <ul class="nav justify-content-end">
  <li class="dropdown">
  			  <a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
            </li>
</ul>

</nav>
    <div class="mx-auto my-5" style="width: 200px;">
    <h1 class="font-weight-bold ">ユーザー覧</h1>
    </div >

	<div class="touroku">

    <a href="UserRegistrationServlet" target="_blank" >新規登録</a>

	</div>

     <div class="panel-body">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">検索条件</div>
            </div>
            <div class="panel-body">
              <form method="post" action="UserListServlet" class="form-horizontal">
                <div class="form-group">
                  <label for="code" class="control-label col-sm-2">ログインID</label>
                  <div class="col-sm-6">
                    <input type="search" name="login-id" id="login-id" class="form-control"/>

                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="control-label col-sm-2">ユーザ名</label>
                  <div class="col-sm-6">
                    <input type="search" name="user-name" id="user-name" class="form-control"/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="continent" class="control-label col-sm-2">生年月日</label>
                  <div class="row">
                    <div class="col-sm-2">
                      <input type="date" name="date-start" id="date-start" class="form-control" size="30"/>
                    </div>
                    <div class="col-xs-1 text-center">
                      ~
                    </div>
                    <div class="col-sm-2">
                      <input type="date" name="date-end" id="date-end" class="form-control"/>
                    </div>
                </div>
                </div>
                <div class="text-right">
                  <button type="submit" value="検索" class="btn btn-primary form-submit">検索</button>
                </div>
              </form>
            </div>
        </div>

        <div class="table-responsive">
             <table class="table table-striped">
               <thead>
                 <tr>
                   <th>ログインID</th>
                   <th>ユーザ名</th>
                   <th>生年月日</th>
                   <th></th>
                 </tr>
               </thead>
               <tbody>
                 <c:forEach var="user" items="${userList}" >
                   <tr>
                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>
                     <!-- TODO 未実装；ログインボタンの表示制御を行う -->
                     <td>
                       <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>

						<c:if test="${userInfo.loginId == 'admin' || userInfo.loginId == user.loginId}">
                       <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
                       </c:if>

                       <c:if test="${userInfo.loginId == 'admin'}">
                       <a class="btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>
                       </c:if>

                     </td>
                   </tr>
                 </c:forEach>
               </tbody>
             </table>
           </div>




    </body>


</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>ユーザー情報更新</title>
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
	integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2"
	crossorigin="anonymous">

<style>
.abcd {
	text-align: center;
}
</style>
</head>

<body>
	<nav class="navbar navbar-light bg-light">
		<span class="navbar-brand mb-0 h1">${userInfo.name} さん</span>
		<ul class="nav justify-content-end">
			<li class="dropdown"><a href="LogoutServlet"
				class="navbar-link logout-link">ログアウト</a></li>
		</ul>

	</nav>
	<div class="mx-auto my-5" style="width: 400px;">
		<h1 class="font-weight-bold ">ユーザー情報更新</h1>
	</div>

	<form action="UserUpdateServlet" method="post">
		<input type="hidden" name="id" value=${user.id } size="15"
			maxlength="20">
		<h2 class="col-md mt-">ログインID ${user.loginId }</h2>
		<br>

		<h2 class="col-md ">
			パスワード <input type="password" name="password" size="15" maxlength="20">
		</h2>
		<br>

		<h2 class="col-md ">
			パスワード(確認) <input type="password" name="password1" size="15"
				maxlength="20">
		</h2>
		<br>

		<h2 class="col-md mt-">
			ユーザー名 <input type="text" name="name" value=${user.name }>
		</h2>
		<br>

		<h2 class="col-md mt-">
			生年月日 <input type="text" name="birth_date" value=${user.birthDate }>
		</h2>
		<br>

		<div class="abcd">
			<input type="submit" value="更新" class="mx-auto" style="width: 200px;">
		</div>
	</form>
	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>
	<br>
	<div class="ml-5">
		<a href="UserListServlet" target="_blank" class=>戻る</a>
	</div>


</body>


</html>
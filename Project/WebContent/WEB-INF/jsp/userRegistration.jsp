<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>ユーザー新規登録</title>
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
	integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2"
	crossorigin="anonymous">

<style>
.abc {
	text-align: center;
}
</style>
</head>

<body>
	<nav class="navbar navbar-light bg-light">
		<span class="navbar-brand mb-0 h1">${userInfo.name} さん</span>
		<ul class="nav justify-content-end">
			<li class="nav-item"><a class="nav-link active"
				href="LogoutServlet" class="navbar-link logout-link">ログアウト</a></li>
		</ul>
	</nav>
	<form method="post" action="UserRegistrationServlet">
		<div class="mx-auto my-5" style="width: 400px;">
			<h1 class="font-weight-bold ">ユーザー新規登録</h1>
		</div>

		<h2 class="col-md mt-">
			ログインID <input type="text" name="loginid" id="login-id"
				class="form-control" />
		</h2>
		<br>

		<h2 class="col-md ">
			パスワード <input type="password" name="password" id="password"
				class="form-control" size="15" maxlength="20">
		</h2>
		<br>

		<h2 class="col-md ">
			パスワード(確認) <input type="password" name="password1" id="password"
				class="form-control" size="15" maxlength="20">
		</h2>
		<br>

		<h2 class="col-md mt-">
			ユーザー名 <input type="text" name="name" id="user-name"
				class="form-control">
		</h2>
		<br>

		<h2 class="col-md mt-">
			生年月日 <input type="text" name="birth_date" id="birth_date"
				class="form-control">
		</h2>
		<br>
		<div class="abc">
			<input type="submit" value="登録" class="mx-auto" style="width: 200px;">
		</div>
	</form>
	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>



	<div class="ml-5">
		<a href="UserListServlet" target="_blank" class=>戻る</a>
	</div>

</body>


</html>
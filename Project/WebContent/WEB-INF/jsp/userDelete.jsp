<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>ユーザー削除確認</title>
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
	integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2"
	crossorigin="anonymous">

<style>
.aaa {
	text-align: center;
}
</style>

</head>

<body>
	<nav class="navbar navbar-light bg-light">
		<span class="navbar-brand mb-0 h1">${userInfo.name} さん</span>
		<ul class="nav justify-content-end">
			<li class="dropdown"><a href="LogoutServlet"
				class="navbar-link logout-link">ログアウト</a></li>
		</ul>

	</nav>


	<div class="mx-auto my-5" style="width: 500px;">
		<h1 class="font-weight-bold ">ユーザー削除確認</h1>
	</div>
	<div class="aaa">
		<h3>ログインID:${user.loginId }</h3>
		<h4>を本当に削除してよろしいでしょうか。</h4>
		<br> <br> <br> <br> <br>

		<form method="post" action="UserDeleteServlet">
			<input type="hidden" name="id" value=${user.id } size="15"
				maxlength="20"> <input type="submit" name="btn" value="OK"
				class="mx-auto" style="width: 200px;">
		</form>
		<form method="get" action="UserListServlet">
			<input type="submit" name="btn" value="キャンセル" class="mx-auto"
				style="width: 200px;">
		</form>
	</div>
</body>


</html>
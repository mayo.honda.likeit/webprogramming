<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>ユーザ情報詳細参照</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  </head>

<body>
  <nav class="navbar navbar-light bg-light">
  <span class="navbar-brand mb-0 h1">${userInfo.name} さん</span>
      <ul class="nav justify-content-end">
  <li class="dropdown">
  			  <a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
            </li>
</ul>

</nav>
    <div class="mx-auto my-5" style="width: 500px;">
    <h1 class="font-weight-bold ">ユーザ情報詳細参照</h1>
    </div >
    <div class="ml-5">


    <h3>ログインID  　　　　　　　　　　${user.loginId }</h3><br>
    <h3>ユーザー名  　　　　　　　　　　${user.name }</h3><br>
    <h3>生年月日  　　　　　　　　　　　${user.birthDate }</h3><br>
    <h3>登録日時  　　　　　　　　　　　${user.createDate }</h3><br>
    <h3>更新日時  　　　　　　　　　　　${user.updateDate }</h3><br>

        <a href="UserListServlet" target="_blank" class=>戻る</a>
    </div>




    </body>


</html>
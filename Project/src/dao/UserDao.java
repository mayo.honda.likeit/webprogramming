package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {

	//ログインIDとパスワードに紐づくユーザ情報を返す
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// 確認済みのSQL
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";



			//暗号化
			String result = seacret(password);

			System.out.println(result);


			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, result);
			ResultSet rs = pStmt.executeQuery();



			// ログイン失敗時の処理
			if (!rs.next()) {
				return null;
			}

			// ログイン成功時の処理
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//全てのユーザー情報の取得
	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			//SELECT文の準備
			String sql = "SELECT * FROM user WHERE id != 1 ";



			//SELECT文を実行し、結果表(ResultSet)を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("create_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

		return userList;

	}

	//ユーザー情報を登録

	public void entry(String loginId, String password, String name, String birth_date) {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// 確認済みのSQL
			String sql = "INSERT INTO user(login_id,password,name,birth_date,create_date,update_date) VALUES(?,?,?,?,NOW(),NOW())";


			//暗号化
			String result = seacret(password);

			System.out.println(result);

			stmt = conn.prepareStatement(sql);
			stmt.setString(1, loginId);
			stmt.setString(2,result);
			stmt.setString(3, name);
			stmt.setString(4, birth_date);

			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	//ユーザー情報参照
	//ログインIDに紐づくユーザ情報を返す
	public User reference(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// 確認済みのSQL
			String sql = "SELECT * FROM user WHERE id = ? ";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);

			ResultSet rs = pStmt.executeQuery();

			// 登録失敗時の処理
			if (!rs.next()) {
				return null;
			}
			// 登録成功時の処理
			int IdData = rs.getInt("id");
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			Date dateData = rs.getDate("birth_date");
			String createData = rs.getString("create_date");
			String updateData = rs.getString("update_date");

			return new User(IdData, loginIdData, nameData, dateData, createData, updateData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}

	//ユーザー情報更新①
	public User renewal(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// 確認済みのSQL
			String sql = "SELECT * FROM user WHERE id = ? ";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1,id);
			ResultSet rs = pStmt.executeQuery();



			// 登録失敗時の処理
			if (!rs.next()) {
				return null;
			}
			// 登録成功時の処理
			String loginIdData = rs.getString("login_id");
			String usernameData = rs.getString("name");
			Date dateData = rs.getDate("birth_date");
			int IdData = rs.getInt("id");

			return new User(loginIdData, usernameData, dateData, IdData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//ユーザー情報更新 ②

	public void koushin(String id, String password, String name, String birth_date) {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// 確認済みのSQL
			String sql = "UPDATE user SET password = ? ,name=? ,birth_date=? WHERE id =?";


			//暗号化
			String result = seacret(password);

			System.out.println(result);


			stmt = conn.prepareStatement(sql);
			stmt.setString(1,result);
			stmt.setString(2, name);
			stmt.setString(3, birth_date);
			stmt.setString(4, id);

			stmt.executeUpdate();



		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}


	//ユーザー情報更新 ③　パスワード抜きの更新

		public void koushinb(String id, String name, String birth_date) {
			Connection conn = null;
			PreparedStatement stmt = null;
			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				// 確認済みのSQL
				String sql = "UPDATE user SET name=? ,birth_date=? WHERE id =?";

				stmt = conn.prepareStatement(sql);
				stmt.setString(1, name);
				stmt.setString(2, birth_date);
				stmt.setString(3, id);

				stmt.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();

			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();

					}
				}
			}
		}



	//ユーザー削除確認①
	public User sakujo(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// 確認済みのSQL
			String sql = "SELECT * FROM user WHERE id = ? ";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);

			ResultSet rs = pStmt.executeQuery();

			// 登録失敗時の処理
			if (!rs.next()) {
				return null;
			}
			// 登録成功時の処理
			String loginIdData = rs.getString("login_id");

			return new User(loginIdData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//ユーザー削除確認②
	public void delete(String id) {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// 確認済みのSQL
			String sql = "DELETE FROM user WHERE id = ?";

			stmt = conn.prepareStatement(sql);
			stmt.setString(1, id);

			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	//ログインID　確認メソッド
	public User findByLoginID(String loginId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// 確認済みのSQL
			String sql = "SELECT * FROM user WHERE login_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			// 登録失敗時の処理
			if (!rs.next()) {
				return null;
			}

			// 登録成功時の処理
			String loginIdData = rs.getString("login_id");
			return new User(loginIdData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//検索メソッド

	//ユーザー情報の取得
	public List<User> findbySearch(String loginid,String name,String startDate,String endDate){
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			//SELECT文の準備
			String sql = "SELECT * FROM user WHERE id != 1";

			if(!loginid.equals("")) {
				sql += " AND login_id = '" + loginid + "'";
			}
			if(!name.equals("")) {
				sql += " AND name LIKE '%" + name + "%'" ;
			}
			if(!startDate.equals("")) {
				sql += " AND birth_date >= '" + startDate + "'" ;
			}
			if(!endDate.equals("")) {
				sql += " AND birth_date <=  '" + endDate + "'" ;
			}

			//SELECT文を実行し、結果表(ResultSet)を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String username = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("create_date");

				User user = new User(id, loginId, username, birthDate, password, createDate, updateDate);

				userList.add(user);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

		return userList;

	}



	//暗号化メソッド
	public String seacret(String password) {
		//ハッシュを生成したい元の文字列
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);
		//標準出力
		return result;



	}

}

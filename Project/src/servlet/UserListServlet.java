package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserListServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//ユーザー一覧の情報を取得
		UserDao userDao = new UserDao();
		List<User> userList = userDao.findAll();

		//ログイン情報がない場合はログイン画面に
		//ある場合はその画面に

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("userInfo");
		if (user == null) {
			// フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);

		} else {
			//リクエストスコープにユーザー一覧情報をセット
			request.setAttribute("userList", userList);

			//ユーザー一覧のjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
			dispatcher.forward(request, response);

		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// formからここに飛ぶ
		//ユーザー一覧のjspにフォワードする
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		//ユーザー一覧情報を取得
		UserDao userDao = new UserDao();
		String loginid = request.getParameter("login-id");
		String name = request.getParameter("user-name");
		String startDate = request.getParameter("date-start");
		String endDate = request.getParameter("date-end");

		List<User> userList = userDao.findbySearch(loginid, name, startDate, endDate);

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("userList", userList);

		// ユーザ一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
		dispatcher.forward(request, response);

	}

}



package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserRegistration
 */
@WebServlet("/UserRegistrationServlet")
public class UserRegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserRegistrationServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//ログイン情報がない場合はログイン画面に
		//ある場合はその画面に

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("userInfo");
		if (user == null) {
			// フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		} else {

			// フォワードでuserRegistration.jspに飛ぶ
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userRegistration.jsp");
			dispatcher.forward(request, response);
			return;

		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//formからここに飛ぶ

		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("loginid");
		String password = request.getParameter("password");
		String password1 = request.getParameter("password1");
		String name = request.getParameter("name");
		String birth_date = request.getParameter("birth_date");

		String str = "";

		//パスワードとパスワード(確認)が異なる場合
		if (!(password.equals(password1))) {
			// リクエストスコープにエラーメッセージ
			request.setAttribute("errMsg", "入力された内容は正しくありません。");
			// 新規登録jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userRegistration.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//入力項目に1つでも未入力がある場合
		if (loginId.equals(str) || password.equals(str) || password1.equals(str) || name.equals(str)
				|| birth_date.equals(str)) {
			// リクエストスコープにエラーメッセージ
			request.setAttribute("errMsg", "入力された内容は正しくありません。");
			// 新規登録jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userRegistration.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//既に登録されているログインIDが入力された場合

		//daoのメソッド実行
		UserDao userDao = new UserDao();
		User user = userDao.findByLoginID(loginId);

		if (!(user == null)) {
			// リクエストスコープにエラーメッセージ
			request.setAttribute("errMsg", "入力された内容は正しくありません。");
			// 新規登録jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userRegistration.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//daoのメソッド実行
		UserDao userDao1 = new UserDao();
		userDao1.entry(loginId, password1, name, birth_date);

		//暗号化
				UserDao userDao2 = new UserDao();
				userDao2.seacret(password);

		//登録成功時の処理
		// ユーザ一覧のサーブレットにリダイレクト
		response.sendRedirect("UserListServlet");

	}

}

package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//ログイン情報がない場合はログイン画面に
		//ある場合はその画面に

		HttpSession session = request.getSession();
		User user1 = (User) session.getAttribute("userInfo");
		if (user1 == null) {
			// フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);

		} else {
			// URLからGETパラメータとしてIDを受け取る
			String id = request.getParameter("id");

			// 確認 idをコンソールに出力
			System.out.println(id);

			// idを引数にして、idに紐づくユーザ情報を出力する

			UserDao userDao = new UserDao();
			User user = userDao.renewal(id);

			// ユーザ情報をリクエストスコープにセットしてjspにフォワード

			request.setAttribute("user", user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);

		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//formからここに飛ぶ

		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String password = request.getParameter("password");
		String password1 = request.getParameter("password1");
		String name = request.getParameter("name");
		String birth_date = request.getParameter("birth_date");
		String id = request.getParameter("id");

		String str = "";

		//パスワード以外の更新
		if (password.equals(str) || password1.equals(str)) {

			//daoのメソッド実行 パスワード以外
			UserDao userDao2 = new UserDao();
			userDao2.koushinb(id, name, birth_date);

			//暗号化
			UserDao userDao = new UserDao();
			userDao.seacret(password);

			// ユーザ一覧のサーブレットにリダイレクト
			response.sendRedirect("UserListServlet");
			return;
		}

		//パスワードとパスワード(確認)が異なる場合
		if (!(password.equals(password1))) {
			// idを引数にして、idに紐づくユーザ情報を出力する

			UserDao userDao = new UserDao();
			User user = userDao.renewal(id);

			// ユーザ情報をリクエストスコープにセットしてjspにフォワード

			request.setAttribute("user", user);

			// リクエストスコープにエラーメッセージ
			request.setAttribute("errMsg", "入力された内容は正しくありません。");
			// 情報更新jspにフォワード
			RequestDispatcher dispatcher1 = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher1.forward(request, response);
			return;

		}

		//パスワード以外で未入力がある場合
		if (name.equals(str) || birth_date.equals(str)) {
			// idを引数にして、idに紐づくユーザ情報を出力する

			UserDao userDao = new UserDao();
			User user = userDao.renewal(id);

			// ユーザ情報をリクエストスコープにセットしてjspにフォワード

			request.setAttribute("user", user);

			// リクエストスコープにエラーメッセージ
			request.setAttribute("errMsg", "入力された内容は正しくありません。");
			// 情報更新jspにフォワード
			RequestDispatcher dispatcher2 = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher2.forward(request, response);
			return;

		}

		//daoのメソッド実行 全部更新
		UserDao userDao = new UserDao();
		userDao.koushin(id, password, name, birth_date);

		//暗号化
		UserDao userDao1 = new UserDao();
		userDao1.seacret(password);



		// ユーザ一覧のサーブレットにリダイレクト
		response.sendRedirect("UserListServlet");

	}

}
